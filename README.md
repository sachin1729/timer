# timer

# timer-basic

A simple timer package that enables recording times(in **milliseconds**) between multiple events

## Install
```
npm install --save timer-basic
```

## Usage

### Creation
```js
const timer = require('timer-basic');
```

### .start()
Starts the timer and creates the unique *handle* for a particular timer instance. Optionally, a *handle* argument(string) can be passed ,which will register the timer as the argument .  The *handle* can be used in subsequent methods.

```js
var handle = timer.start();
```

### .pause(handle)
pauses the timer.

```js
console.log(timer.pause(handle)) //Single integer if there was no pause/resume before this,
// otherwise array of intervals for which timer was running
```

### .resume(handle)
resumes the timer. The module will start a new interval.

```js
timer.resume(handle)
```

### .isRunning(handle)
checks if the timer is running

```js
console.log(timer.isRunning(handle)); // true
```

### .stop(handle)
stops the timer and deletes the handle. Returns the time interval(s) for which timer was running.

```js
var interval = timer.stop(handle);
console.log(interval)   //Single integer if there was no pause/resume,
// otherwise array of intervals for which timer was running
```

## License
The ISC License. Full License is [here](https://bitbucket.org/sachin1729/timer/src/master/LICENSE)

